#ifndef PDAnalysis_Ntu_PDReducedNtupleData_h
#define PDAnalysis_Ntu_PDReducedNtupleData_h

#include "NtuTool/Common/interface/TreeWrapper.h"
#include <vector>
#include "RtypesCore.h"

class PDReducedNtupleData: public virtual TreeWrapper
{

 public:

  void Reset() { autoReset(); }

  PDReducedNtupleData() {}
  ~PDReducedNtupleData() override {}

  void initTree()
  {

    treeName = "analyzerTree";

    // EVT
    setBranch("run", &run, "run/D", &b_run);
    setBranch("evt", &evt, "evt/l", &b_evt);
    setBranch("lumi", &lumi, "lumi/D", &b_lumi);

    setBranch("isBs", &isBs, "isBs/I", &b_isBs);
    setBranch("nPV", &nPV, "nPV/I", &b_nPV);
    setBranch("nBs_JpsiMu", &nBs_JpsiMu, "nBs_JpsiMu/I", &b_nBs_JpsiMu);
    setBranch("nBs_JpsiTrkTrk", &nBs_JpsiTrkTrk, "nBs_JpsiTrkTrk/I", &b_nBs_JpsiTrkTrk);

    // HLT
    setBranch("hlt_JpsiTrkTrk", &hlt_JpsiTrkTrk, "hlt_JpsiTrkTrk/I", &b_hlt_JpsiTrkTrk);
    setBranch("hlt_JpsiTrk", &hlt_JpsiTrk, "hlt_JpsiTrk/I", &b_hlt_JpsiTrk);
    setBranch("hlt_JpsiMu", &hlt_JpsiMu, "hlt_JpsiMu/I", &b_hlt_JpsiMu);

    setBranch("passJpsiMuSel", &passJpsiMuSel, "passJpsiMuSel/I", &b_passJpsiMuSel);
    setBranch("passJpsiTrkTrkSel", &passJpsiTrkTrkSel, "passJpsiTrkTrkSel/I", &b_passJpsiTrkTrkSel);

    setBranch("hltMatch_Mu", &hltMatch_Mu, "hltMatch_Mu/I", &b_hltMatch_Mu);
    setBranch("hltMatch_Tracks", &hltMatch_Tracks, "hltMatch_Tracks/I", &b_hltMatch_Tracks);

    // BS
    // setBranch("pv_Pointing", &pv_Pointing, "pv_Pointing/D", &b_pv_Pointing);
    setBranch("pv_VtxProb", &pv_VtxProb, "pv_VtxProb/D", &b_pv_VtxProb);
    // setBranch("bs_Mass", &bs_Mass, "bs_Mass/D", &b_bs_Mass);
    setBranch("bs_MassFromSV", &bs_MassFromSV, "bs_MassFromSV/D", &b_bs_MassFromSV);
    // setBranch("bs_MassNoRefit", &bs_MassNoRefit, "bs_MassNoRefit/D", &b_bs_MassNoRefit);
    setBranch("bs_Pt", &bs_Pt, "bs_Pt/D", &b_bs_Pt);
    setBranch("bs_Eta", &bs_Eta, "bs_Eta/D", &b_bs_Eta);
    setBranch("bs_Phi", &bs_Phi, "bs_Phi/D", &b_bs_Phi);

    // LIFETIME 
    setBranch("ctBS", &ctBS, "ctBS/D", &b_ctBS);
    setBranch("ctBS_Err", &ctBS_Err, "ctBS_Err/D", &b_ctBS_Err);

    setBranch("ctPVpointRefit", &ctPVpointRefit, "ctPVpointRefit/D", &b_ctPVpointRefit);
    setBranch("ctPVpointRefit_Err", &ctPVpointRefit_Err, "ctPVpointRefit_Err/D", &b_ctPVpointRefit_Err);
    setBranch("ctPVdistRefit", &ctPVdistRefit, "ctPVdistRefit/D", &b_ctPVdistRefit);
    setBranch("ctPVdistRefit_Err", &ctPVdistRefit_Err, "ctPVdistRefit_Err/D", &b_ctPVdistRefit_Err);

    setBranch("ctPVpoint", &ctPVpoint, "ctPVpoint/D", &b_ctPVpoint);
    setBranch("ctPVpoint_Err", &ctPVpoint_Err, "ctPVpoint_Err/D", &b_ctPVpoint_Err);
    setBranch("ctPVdist", &ctPVdist, "ctPVdist/D", &b_ctPVdist);
    setBranch("ctPVdist_Err", &ctPVdist_Err, "ctPVdist_Err/D", &b_ctPVdist_Err);

    // ANGLES
    setBranch("angle_CosPsi", &angle_CosPsi, "angle_CosPsi/D", &b_angle_CosPsi);
    setBranch("angle_CosTheta", &angle_CosTheta, "angle_CosTheta/D", &b_angle_CosTheta);
    setBranch("angle_Phi", &angle_Phi, "angle_Phi/D", &b_angle_Phi);

    // TAG
    setBranch("tag_BdMass", &tag_BdMass, "tag_BdMass/I", &b_tag_BdMass);

    setBranch("tag_Mu", &tag_Mu, "tag_Mu/I", &b_tag_Mu);
    setBranch("mistag_Mu1", &mistag_Mu1, "mistag_Mu1/D", &b_mistag_Mu1);
    setBranch("mistagCal_Mu1", &mistagCal_Mu1, "mistagCal_Mu1/D", &b_mistagCal_Mu1);
    setBranch("mistag_Mu2", &mistag_Mu2, "mistag_Mu2/D", &b_mistag_Mu2);
    setBranch("mistagCal_Mu2", &mistagCal_Mu2, "mistagCal_Mu2/D", &b_mistagCal_Mu2);

    setBranch("tag_Ele", &tag_Ele, "tag_Ele/I", &b_tag_Ele);
    setBranch("mistag_Ele", &mistag_Ele, "mistag_Ele/D", &b_mistag_Ele);
    setBranch("mistagCal_Ele", &mistagCal_Ele, "mistagCal_Ele/D", &b_mistagCal_Ele);

    setBranch("tag_Jet", &tag_Jet, "tag_Jet/I", &b_tag_Jet);
    setBranch("mistag_Jet", &mistag_Jet, "mistag_Jet/D", &b_mistag_Jet);
    setBranch("mistagCal_Jet", &mistagCal_Jet, "mistagCal_Jet/D", &b_mistagCal_Jet);

    setBranch("tag_SS", &tag_SS, "tag_SS/I", &b_tag_SS);
    setBranch("mistag_SS", &mistag_SS, "mistag_SS/D", &b_mistag_SS);
    setBranch("mistagCal_SS", &mistagCal_SS, "mistagCal_SS/D", &b_mistagCal_SS);
    setBranch("tag_SS_commonTrk", &tag_SS_commonTrk, "tag_SS_commonTrk/I", &b_tag_SS_commonTrk);

    // JPSI
    setBranch("jpsi_VtxProb", &jpsi_VtxProb, "jpsi_VtxProb/D", &b_jpsi_VtxProb);
    // setBranch("jpsi_Mass", &jpsi_Mass, "jpsi_Mass/D", &b_jpsi_Mass);
    setBranch("jpsi_MassFromSV", &jpsi_MassFromSV, "jpsi_MassFromSV/D", &b_jpsi_MassFromSV);
    // setBranch("jpsi_MassNoRefit", &jpsi_MassNoRefit, "jpsi_MassNoRefit/D", &b_jpsi_MassNoRefit);
    setBranch("jpsi_Pt", &jpsi_Pt, "jpsi_Pt/D", &b_jpsi_Pt);
    setBranch("jpsi_Eta", &jpsi_Eta, "jpsi_Eta/D", &b_jpsi_Eta);
    setBranch("jpsi_Phi", &jpsi_Phi, "jpsi_Phi/D", &b_jpsi_Phi);

    // PHI
    setBranch("phi_VtxProb", &phi_VtxProb, "phi_VtxProb/D", &b_phi_VtxProb);
    // setBranch("phi_Mass", &phi_Mass, "phi_Mass/D", &b_phi_Mass);
    setBranch("phi_MassFromSV", &phi_MassFromSV, "phi_MassFromSV/D", &b_phi_MassFromSV);
    // setBranch("phi_MassNoRefit", &phi_MassNoRefit, "phi_MassNoRefit/D", &b_phi_MassNoRefit);
    setBranch("phi_Pt", &phi_Pt, "phi_Pt/D", &b_phi_Pt);
    setBranch("phi_Eta", &phi_Eta, "phi_Eta/D", &b_phi_Eta);
    setBranch("phi_Phi", &phi_Phi, "phi_Phi/D", &b_phi_Phi);

    // MUONS
    setBranch("muonSoftID", &muonSoftID, "muonSoftID/I", &b_muonSoftID);
    setBranch("muonLooseID", &muonLooseID, "muonLooseID/I", &b_muonLooseID);
    setBranch("muonMediumID", &muonMediumID, "muonMediumID/I", &b_muonMediumID);

    setBranch("muP_Pt", &muP_Pt, "muP_Pt/D", &b_muP_Pt);
    setBranch("muP_Eta", &muP_Eta, "muP_Eta/D", &b_muP_Eta);
    setBranch("muP_Phi", &muP_Phi, "muP_Phi/D", &b_muP_Phi);
    setBranch("muP_HltPt", &muP_HltPt, "muP_HltPt/D", &b_muP_HltPt);
    setBranch("muP_Hits", &muP_Hits, "muP_Hits/I", &b_muP_Hits);

    setBranch("muM_Pt", &muM_Pt, "muM_Pt/D", &b_muM_Pt);
    setBranch("muM_Eta", &muM_Eta, "muM_Eta/D", &b_muM_Eta);
    setBranch("muM_Phi", &muM_Phi, "muM_Phi/D", &b_muM_Phi);
    setBranch("muM_HltPt", &muM_HltPt, "muM_HltPt/D", &b_muM_HltPt);
    setBranch("muM_Hits", &muM_Hits, "muM_Hits/I", &b_muM_Hits);

    // TRACKS
    setBranch("kaonP_Pt", &kaonP_Pt, "kaonP_Pt/D", &b_kaonP_Pt);
    setBranch("kaonP_Eta", &kaonP_Eta, "kaonP_Eta/D", &b_kaonP_Eta);
    setBranch("kaonP_Phi", &kaonP_Phi, "kaonP_Phi/D", &b_kaonP_Phi);
    setBranch("kaonP_Hits", &kaonP_Hits, "kaonP_Hits/I", &b_kaonP_Hits);
    
    setBranch("kaonM_Pt", &kaonM_Pt, "kaonM_Pt/D", &b_kaonM_Pt);
    setBranch("kaonM_Eta", &kaonM_Eta, "kaonM_Eta/D", &b_kaonM_Eta);
    setBranch("kaonM_Phi", &kaonM_Phi, "kaonM_Phi/D", &b_kaonM_Phi);
    setBranch("kaonM_Hits", &kaonM_Hits, "kaonM_Hits/I", &b_kaonM_Hits);
    
    setBranch("kaon_DeltaDz", &kaon_DeltaDz, "kaon_DeltaDz/D", &b_kaon_DeltaDz);
    setBranch("kaon_DeltaR", &kaon_DeltaR, "kaon_DeltaR/D", &b_kaon_DeltaR);

    // GENERATION INFORMATION
    setBranch("gen_GenFound", &gen_GenFound, "gen_GenFound/I", &b_gen_GenFound);
    setBranch("gen_LundCode", &gen_LundCode, "gen_LundCode/I", &b_gen_LundCode);
    setBranch("gen_IsBMixed", &gen_IsBMixed, "gen_IsBMixed/I", &b_gen_IsBMixed);
    setBranch("gen_Matched", &gen_Matched, "gen_Matched/I", &b_gen_Matched);

    setBranch("bs_Mass_GEN", &bs_Mass_GEN, "bs_Mass_GEN/D", &b_bs_Mass_GEN);
    setBranch("bs_Pt_GEN", &bs_Pt_GEN, "bs_Pt_GEN/D", &b_bs_Pt_GEN);
    setBranch("bs_Eta_GEN", &bs_Eta_GEN, "bs_Eta_GEN/D", &b_bs_Eta_GEN);
    setBranch("bs_Phi_GEN", &bs_Phi_GEN, "bs_Phi_GEN/D", &b_bs_Phi_GEN);

    setBranch("ct_GEN", &ct_GEN, "ct_GEN/D", &b_ct_GEN);
    setBranch("ct3D_GEN", &ct3D_GEN, "ct3D_GEN/D", &b_ct3D_GEN);

    setBranch("angle_CosPsi_GEN", &angle_CosPsi_GEN, "angle_CosPsi_GEN/D", &b_angle_CosPsi_GEN);
    setBranch("angle_CosTheta_GEN", &angle_CosTheta_GEN, "angle_CosTheta_GEN/D", &b_angle_CosTheta_GEN);
    setBranch("angle_Phi_GEN", &angle_Phi_GEN, "angle_Phi_GEN/D", &b_angle_Phi_GEN);

    setBranch("jpsi_Mass_GEN", &jpsi_Mass_GEN, "jpsi_Mass_GEN/D", &b_jpsi_Mass_GEN);
    setBranch("jpsi_Pt_GEN", &jpsi_Pt_GEN, "jpsi_Pt_GEN/D", &b_jpsi_Pt_GEN);
    setBranch("jpsi_Eta_GEN", &jpsi_Eta_GEN, "jpsi_Eta_GEN/D", &b_jpsi_Eta_GEN);
    setBranch("jpsi_Phi_GEN", &jpsi_Phi_GEN, "jpsi_Phi_GEN/D", &b_jpsi_Phi_GEN);

    setBranch("phi_Mass_GEN", &phi_Mass_GEN, "phi_Mass_GEN/D", &b_phi_Mass_GEN);
    setBranch("phi_Pt_GEN", &phi_Pt_GEN, "phi_Pt_GEN/D", &b_phi_Pt_GEN);
    setBranch("phi_Eta_GEN", &phi_Eta_GEN, "phi_Eta_GEN/D", &b_phi_Eta_GEN);
    setBranch("phi_Phi_GEN", &phi_Phi_GEN, "phi_Phi_GEN/D", &b_phi_Phi_GEN);

    setBranch("muP_Pt_GEN", &muP_Pt_GEN, "muP_Pt_GEN/D", &b_muP_Pt_GEN);
    setBranch("muP_Eta_GEN", &muP_Eta_GEN, "muP_Eta_GEN/D", &b_muP_Eta_GEN);
    setBranch("muP_Phi_GEN", &muP_Phi_GEN, "muP_Phi_GEN/D", &b_muP_Phi_GEN);
    setBranch("muM_Pt_GEN", &muM_Pt_GEN, "muM_Pt_GEN/D", &b_muM_Pt_GEN);
    setBranch("muM_Eta_GEN", &muM_Eta_GEN, "muM_Eta_GEN/D", &b_muM_Eta_GEN);
    setBranch("muM_Phi_GEN", &muM_Phi_GEN, "muM_Phi_GEN/D", &b_muM_Phi_GEN);

    setBranch("kaonP_Pt_GEN", &kaonP_Pt_GEN, "kaonP_Pt_GEN/D", &b_kaonP_Pt_GEN);
    setBranch("kaonP_Eta_GEN", &kaonP_Eta_GEN, "kaonP_Eta_GEN/D", &b_kaonP_Eta_GEN);
    setBranch("kaonP_Phi_GEN", &kaonP_Phi_GEN, "kaonP_Phi_GEN/D", &b_kaonP_Phi_GEN);
    setBranch("kaonM_Pt_GEN", &kaonM_Pt_GEN, "kaonM_Pt_GEN/D", &b_kaonM_Pt_GEN);
    setBranch("kaonM_Eta_GEN", &kaonM_Eta_GEN, "kaonM_Eta_GEN/D", &b_kaonM_Eta_GEN);
    setBranch("kaonM_Phi_GEN", &kaonM_Phi_GEN, "kaonM_Phi_GEN/D", &b_kaonM_Phi_GEN);

  }

  // EVT
  double run, lumi;
  ULong64_t evt;
  int isBs, nPV, nBs_JpsiMu, nBs_JpsiTrkTrk;

  // HLT
  int hlt_JpsiTrkTrk, hlt_JpsiTrk, hlt_JpsiMu, hltMatch_Mu, hltMatch_Tracks;
  int passJpsiMuSel, passJpsiTrkTrkSel;

  // BS
  double pv_Pointing, pv_VtxProb, bs_Mass, bs_MassFromSV, bs_MassNoRefit;
  double bs_Pt, bs_Eta, bs_Phi;

  // LIFETIME 
  double ctBS, ctBS_Err;
  double ctPVpointRefit, ctPVpointRefit_Err;
  double ctPVdistRefit, ctPVdistRefit_Err;
  double ctPVpoint, ctPVpoint_Err;
  double ctPVdist, ctPVdist_Err;
  
  // ANGLES
  double angle_CosPsi, angle_CosTheta, angle_Phi;

  // TAG
  int tag_Mu, tag_Ele, tag_Jet, tag_SS, tag_BdMass, tag_SS_commonTrk;
  double mistag_Mu1, mistagCal_Mu1, mistag_Mu2, mistagCal_Mu2;
  double mistag_Ele, mistagCal_Ele, mistag_Jet;
  double mistagCal_Jet, mistag_SS, mistagCal_SS;

  // JPSI
  double jpsi_VtxProb, jpsi_Mass, jpsi_MassFromSV, jpsi_MassNoRefit;
  double jpsi_Pt, jpsi_Eta, jpsi_Phi;

  // PHI
  double phi_VtxProb, phi_Mass, phi_MassFromSV, phi_MassNoRefit;
  double phi_Pt, phi_Eta, phi_Phi;

  // MUONS
  int muonSoftID, muonLooseID, muonMediumID, muP_Hits, muM_Hits;
  double muP_Pt, muP_Eta, muP_Phi, muP_HltPt, muM_Pt, muM_Eta, muM_Phi, muM_HltPt;

  // TRACKS
  int kaonP_Hits, kaonM_Hits;
  double kaonP_Pt, kaonP_Eta, kaonP_Phi, kaonM_Pt, kaonM_Eta, kaonM_Phi;
  double kaon_DeltaDz, kaon_DeltaR;

  // GEN
  int gen_GenFound, gen_LundCode, gen_Matched, gen_IsBMixed;
  double bs_Mass_GEN, bs_Pt_GEN, bs_Eta_GEN, bs_Phi_GEN;
  double ct_GEN, ct3D_GEN;
  double angle_CosPsi_GEN, angle_CosTheta_GEN, angle_Phi_GEN;
  double jpsi_Mass_GEN, jpsi_Pt_GEN, jpsi_Eta_GEN, jpsi_Phi_GEN;
  double phi_Mass_GEN, phi_Pt_GEN, phi_Eta_GEN, phi_Phi_GEN;
  double muP_Pt_GEN, muP_Eta_GEN, muP_Phi_GEN;
  double muM_Pt_GEN, muM_Eta_GEN, muM_Phi_GEN;
  double kaonP_Pt_GEN, kaonP_Eta_GEN, kaonP_Phi_GEN;
  double kaonM_Pt_GEN, kaonM_Eta_GEN, kaonM_Phi_GEN;

  // BRANCHES
  TBranch *b_run, *b_lumi, *b_evt, *b_isBs, *b_nPV;
  TBranch *b_nBs_JpsiMu, *b_nBs_JpsiTrkTrk;
  TBranch *b_hlt_JpsiTrkTrk, *b_hlt_JpsiTrk, *b_hlt_JpsiMu;
  TBranch *b_hltMatch_Mu, *b_hltMatch_Tracks;
  TBranch *b_passJpsiMuSel, *b_passJpsiTrkTrkSel;
  TBranch *b_pv_Pointing, *b_pv_VtxProb;
  TBranch *b_bs_Mass, *b_bs_MassFromSV, *b_bs_MassNoRefit;
  TBranch *b_bs_Pt, *b_bs_Eta, *b_bs_Phi;
  TBranch *b_ctPVpointRefit, *b_ctPVpointRefit_Err;
  TBranch *b_ctBS, *b_ctBS_Err;
  TBranch *b_ctPVdistRefit, *b_ctPVdistRefit_Err;
  TBranch *b_ctPVpoint, *b_ctPVpoint_Err;
  TBranch *b_ctPVdist, *b_ctPVdist_Err;
  TBranch *b_angle_CosPsi, *b_angle_CosTheta, *b_angle_Phi;
  TBranch *b_tag_Mu, *b_tag_Ele, *b_tag_Jet, *b_tag_SS;
  TBranch *b_tag_BdMass, *b_tag_SS_commonTrk;
  TBranch *b_mistag_Mu1, *b_mistagCal_Mu1, *b_mistag_Mu2, *b_mistagCal_Mu2;
  TBranch *b_mistag_Ele, *b_mistagCal_Ele, *b_mistag_Jet, *b_mistagCal_Jet;
  TBranch *b_mistag_SS, *b_mistagCal_SS;
  TBranch *b_jpsi_VtxProb, *b_jpsi_Mass, *b_jpsi_MassFromSV, *b_jpsi_MassNoRefit;
  TBranch *b_jpsi_Pt, *b_jpsi_Eta, *b_jpsi_Phi;
  TBranch *b_phi_VtxProb, *b_phi_Mass, *b_phi_MassFromSV, *b_phi_MassNoRefit;
  TBranch *b_phi_Pt, *b_phi_Eta, *b_phi_Phi;
  TBranch *b_muonSoftID, *b_muonLooseID, *b_muonMediumID;
  TBranch *b_muP_Pt, *b_muP_Eta, *b_muP_Phi, *b_muP_Hits, *b_muP_HltPt;
  TBranch *b_muM_Pt, *b_muM_Eta, *b_muM_Phi, *b_muM_Hits, *b_muM_HltPt;
  TBranch *b_kaonP_Pt, *b_kaonP_Eta, *b_kaonP_Phi, *b_kaonP_Hits;
  TBranch *b_kaonM_Pt, *b_kaonM_Eta, *b_kaonM_Phi, *b_kaonM_Hits;
  TBranch *b_kaon_DeltaDz, *b_kaon_DeltaR;

  TBranch *b_gen_GenFound, *b_gen_LundCode, *b_gen_Matched, *b_gen_IsBMixed;
  TBranch *b_bs_Mass_GEN, *b_bs_Pt_GEN, *b_bs_Eta_GEN, *b_bs_Phi_GEN;
  TBranch *b_ct_GEN, *b_ct3D_GEN;
  TBranch *b_angle_CosPsi_GEN, *b_angle_CosTheta_GEN, *b_angle_Phi_GEN;
  TBranch *b_jpsi_Mass_GEN, *b_jpsi_Pt_GEN, *b_jpsi_Eta_GEN, *b_jpsi_Phi_GEN;
  TBranch *b_phi_Mass_GEN, *b_phi_Pt_GEN, *b_phi_Eta_GEN, *b_phi_Phi_GEN;
  TBranch *b_muP_Pt_GEN, *b_muP_Eta_GEN, *b_muP_Phi_GEN;
  TBranch *b_muM_Pt_GEN, *b_muM_Eta_GEN, *b_muM_Phi_GEN;
  TBranch *b_kaonP_Pt_GEN, *b_kaonP_Eta_GEN, *b_kaonP_Phi_GEN;
  TBranch *b_kaonM_Pt_GEN, *b_kaonM_Eta_GEN, *b_kaonM_Phi_GEN;


 private:

  PDReducedNtupleData           ( const PDReducedNtupleData& a ) = delete;
  PDReducedNtupleData& operator=( const PDReducedNtupleData& a ) = delete;

};

#endif // PDAnalysis_Ntu_PDReducedNtupleData_h
