#ifndef PDAnalysis_Ntu_PDAnalyzer_h
#define PDAnalysis_Ntu_PDAnalyzer_h

#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
#include <vector>
#include <algorithm>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TDirectory.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "TMatrixTBase.h"
#include "Math/SMatrix.h"
#include "Math/Boost.h"

#include "PDAnalyzerUtil.h"
#include "PDAnalysis/Ntu/interface/PDGenHandler.h"
#include "utils/PDMuonVar.h"
#include "utils/PhisUtil.h"
#include "utils/PDSoftMuonMvaEstimator.h"
#include "utils/OSMuonTag.h"
#include "utils/OSEleTag.h"
#include "utils/OSJetTag.h"
#include "utils/SSTag.h"

class PDReducedNtupleWriter;

class PDAnalyzer: public virtual PDAnalyzerUtil
,                 public virtual PDGenHandler
,                 public virtual PDMuonVar
,                 public virtual PhisUtil
,                 public virtual PDSoftMuonMvaEstimator
,                 public virtual OSMuonTag
,                 public virtual OSEleTag
,                 public virtual OSJetTag
,                 public virtual SSTag
 {

 public:

  PDAnalyzer();
  virtual ~PDAnalyzer() override;

  // function called before starting the analysis
  void beginJob() override;

  // functions to book the histograms
  void book() override;

  // functions called for each event
  // function to reset class content before reading from file
  void reset() override;
  // function to do event-by-event analysis,
  // return value "true" for accepted events
  bool analyze( int entry, int event_file, int event_tot ) override;

  // function called at the end of the analysis
  void endJob() override;

  // functions called at the end of the event loop
  using WrapperBase::plot; // needed for the following declaration
//  void plot() override; // plot histograms on the screen (optional, see .cc)
  using WrapperBase::save;
  void save() override; // save histograms on a ROOT file (optional, see .cc)

  bool verbose;

 protected:

  PDReducedNtupleWriter* rWriter;

 private:

  enum HLT {
    JpsiTrk,
    JpsiTrkTrk
  };

  enum CHANNEL {
    Bs,
    Bs_genBd,
    Bs_genLambda,
    Bd
  };

  TString outputFile, process, calibrationOS, calibrationSS, HLTflag;
  bool genOnly, applyOfflineCuts, applyCtCuts, debug, debugGEN;
  HLT HLTTYPE;
  CHANNEL channel;

  int counter[20];

  double phiKxMass = PHIMASS;
  double bMass     = BSMASS;

  TH1F *h1;
  TH1F *h2;
  TH1F *h3;
  TH1F *h4;
  TH1F *h5;

  // dummy copy constructor and assignment
  PDAnalyzer           ( const PDAnalyzer& ) = delete;
  PDAnalyzer& operator=( const PDAnalyzer& ) = delete;

};


#endif // PDAnalysis_Ntu_PDAnalyzer_h

