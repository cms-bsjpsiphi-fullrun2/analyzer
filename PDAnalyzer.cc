#include "PDAnalyzer.h"

// additional features
#include "utils/PhisUtil.cc"
#include "utils/PDMuonVar.cc"
#include "utils/PDSoftMuonMvaEstimator.cc"
#include "utils/OSMuonTag.cc"
#include "utils/OSEleTag.cc"
#include "utils/OSJetTag.cc"
#include "utils/SSTag.cc"

#include <stdexcept>

using namespace std;
using namespace ROOT::Math;

#include "PDReducedNtupleWriter.h"

/*
export PYTHONPATH=$PYTHONPATH:$CMSSW_BASE/src/PDAnalysis/Ntu/bin/utils

pdTreeAnalyze /lustre/cmswork/abragagn/crabProduction/ntuList/charmonium2018UL/Charmonium_Run2018D_DCAP.list hist.root -v outputFile ntu.root -v histoMode RECREATE -v process Bs -v calibrationOS BuData18 -v calibrationSS BsData18 -v applyOfflineCuts t -v applyCtCuts t -v use_gen f -v genOnly f -v debug f -v use_tracks t -v use_hlts t -v use_pvts t -v use_hlto t -v use_info t -n 100000
pdTreeAnalyze /lustre/cmswork/abragagn/crabProduction/ntuList/MC2018UL/BsToJpsiPhi_2018UL_DCAP.list hist.root -v outputFile ntu.root -v histoMode RECREATE -v process Bs -v calibrationOS BsMC18 -v calibrationSS BsMC18 -v applyOfflineCuts t -v applyCtCuts t -v use_gen t -v genOnly f -v debug f -v use_tracks t -v use_hlts t -v use_pvts t -v use_hlto t -v use_info t -n 100000

*/

PDAnalyzer::PDAnalyzer()
{

  cout << "new PDAnalyzer" << endl;

  setUserParameter( "verbose", "f" );
  setUserParameter( "debug", "f" );
  setUserParameter( "debugGEN", "f" );
  setUserParameter( "outputFile", "ntu.root" );
  setUserParameter( "process", "Bs" );
  setUserParameter( "genOnly", "f" );
  setUserParameter( "calibrationOS", "BuData18" );
  setUserParameter( "calibrationSS", "BsData18" );
  setUserParameter( "applyOfflineCuts", "t" );
  setUserParameter( "applyCtCuts", "t" );
  setUserParameter( "HLTflag", "" );

}


PDAnalyzer::~PDAnalyzer()
{}


void PDAnalyzer::beginJob()
{

  PDAnalyzerUtil::beginJob();

  getUserParameter( "verbose", verbose );
  getUserParameter( "debug", debug );
  getUserParameter( "debugGEN", debugGEN );
  getUserParameter( "outputFile", outputFile );
  getUserParameter( "process", process );
  getUserParameter( "genOnly", genOnly );
  getUserParameter( "calibrationOS", calibrationOS );
  getUserParameter( "calibrationSS", calibrationSS );
  getUserParameter( "applyOfflineCuts", applyOfflineCuts );
  getUserParameter( "applyCtCuts", applyCtCuts );
  getUserParameter( "HLTflag", HLTflag );

  cout << "verbose " << verbose << endl;
  cout << "genOnly " << genOnly << endl;
  cout << "outputFile " << outputFile << endl;
  cout << "process " << process << endl;
  cout << "calibrationOS " << calibrationOS << endl;
  cout << "calibrationSS " << calibrationSS << endl;
  cout << "applyOfflineCuts " << applyOfflineCuts << endl;
  cout << "applyCtCuts " << applyCtCuts << endl;
  cout << "HLTflag " << HLTflag << endl;

  // PARAMETERS CHECKS
  if (genOnly and !use_gen)
  {
    throw invalid_argument(" !!!! genOnly and !use_gen !!!! ");
  }

  if (not use_hlto and not genOnly)
  {
    throw invalid_argument("!!!! use_hlto is necessary for reco selection !!!!");
  }

  if (process != "Bs"
    and process != "Bd"
    and process != "Bs_genBd"
    and process != "Bs_genLambda")
  {
    throw invalid_argument("!!! PROCESS WRONG !!!");
  }

  if (process == "Bs")           channel = CHANNEL::Bs;
  if (process == "Bd")           channel = CHANNEL::Bd;
  if (process == "Bs_genBd")     channel = CHANNEL::Bs_genBd;
  if (process == "Bs_genLambda") channel = CHANNEL::Bs_genLambda;

  // Values for Bs are default, see PhisUtil.h
  if (channel == CHANNEL::Bd)
  {
    SetPhiMassWin(kstarMassWin);
    SetBMassMin(BdMassRangeTight[0]);
    SetBMassMax(BdMassRangeTight[1]);
    phiKxMass = KXMASS;
    bMass     = B0MASS;
  }

  if (HLTflag == "JpsiTrkTrk")   HLTTYPE = HLT::JpsiTrkTrk;
  else if (HLTflag == "JpsiTrk") HLTTYPE = HLT::JpsiTrk;
  else
  {
    if (channel == CHANNEL::Bs
      or channel == CHANNEL::Bs_genBd
      or channel == CHANNEL::Bs_genLambda)
    {
      HLTTYPE = HLT::JpsiTrkTrk;
    }

    if (channel == CHANNEL::Bd)
    {
      HLTTYPE = HLT::JpsiTrk;
    }
  }

  // TOOLS INITIALIZATION
  rWriter = new
  PDReducedNtupleWriter;
  rWriter->open( getUserParameter("outputFile"), "RECREATE" );

  // Os Muon
  initializeMuonMvaReader();
  initializeOSMuonReader1();
  initializeOSMuonReader2();
  initializeOSMuonCalibration(calibrationOS);
  initializeOSEleReader();
  initializeOSEleCalibration(calibrationOS);
  initializeOSJetReader();
  initializeOSJetCalibration(calibrationOS);
  initializeSSReader();
  initializeSSCalibration(calibrationSS);

  for (int i = 0; i < 20; ++i) counter[i] = 0;

  return;

}


void PDAnalyzer::book()
{

  autoSavedObject = h1 = new TH1F( "h1", "h1", 25, 0.5, 1. );
  autoSavedObject = h2 = new TH1F( "h2", "h2", 25, 0.5, 1. );
  autoSavedObject = h3 = new TH1F( "h3", "h3", 100, 0., 100. );
  autoSavedObject = h4 = new TH1F( "h4", "h4", 100, 0., 100. );
  autoSavedObject = h5 = new TH1F( "h5", "h5", 100, 0., 100. );

  return;

}


void PDAnalyzer::reset()
{

#  if UTIL_USE == FULL

  autoReset();
#elif UTIL_USE == BARE

#endif
  return;
}


bool PDAnalyzer::analyze( int entry, int event_file, int event_tot )
{

  if (verbose)
  {
    cout << " +++++++++++++++++++++++++++ " << endl;
    cout << "entry: " << entry << " " << event_file << " " << event_tot << endl;
    cout << "run: " <<   runNumber << " , " << "evt: " << eventNumber << endl;
  }
  else
  {
    if ( (!(event_tot % 10) and event_tot < 100 ) or
         (!(event_tot % 100) and event_tot < 1000 ) or
         (!(event_tot % 1000) and event_tot < 10000 ) or
         (!(event_tot % 10000) and event_tot < 100000 ) or
         (!(event_tot % 100000) and event_tot < 1000000 ) or
         (!(event_tot % 1000000) and event_tot < 10000000 ) )
    {
      cout << " == at event " << event_file << " " << event_tot << endl;
    }
  }

  // EVENT INITIALIZATION
  convSpheCart(jetPt, jetEta, jetPhi, jetPx, jetPy, jetPz);
  convSpheCart(muoPt, muoEta, muoPhi, muoPx, muoPy, muoPz);
  convSpheCart(trkPt, trkEta, trkPhi, trkPx, trkPy, trkPz);
  convSpheCart(pfcPt, pfcEta, pfcPhi, pfcPx, pfcPy, pfcPz);

  computeMuonVar();
  initializeOsMuonTagVars();
  initializeOsEleTagVars();
  initializeOSJetTagVars();
  initializeSSTagVars();

  rWriter->Reset();

  if (debug)
  {
    cout << endl << " +++++++++++ NEW EVENT +++++++++++ " << event_tot << endl;
  }

  // Needed in gen block so declared here
  PxPyPzEVector tMuP_reco, tMuM_reco, tKaonP_reco, tKaonM_reco;

  // RECO
  if (!genOnly)
  {

    // TRIGGER SELECTION
    bool hlt_jpsimu     = false;
    bool hlt_jpsitrktrk = false;
    bool hlt_jpsitrk    = false;
    bool hlt_jpsitrk___ = false;

    if (use_hlts)
    {
      if (hlt(PDEnumString::HLT_Dimuon0_Jpsi3p5_Muon2_v))          hlt_jpsimu  = true;
      if (hlt(PDEnumString::HLT_DoubleMu4_JpsiTrkTrk_Displaced_v)) hlt_jpsitrktrk = true;
      if (hlt(PDEnumString::HLT_DoubleMu4_JpsiTrk_Displaced_v))    hlt_jpsitrk = true;

      if (debug)
      {
        cout << " +++++++++++ Checking HLT decision "
             << hlt_jpsimu << " " << hlt_jpsitrktrk << " " << hlt_jpsitrk
             << " +++++++++++ " << endl;
      }

      if (HLTTYPE == HLT::JpsiTrkTrk )   hlt_jpsitrk___ = hlt_jpsitrktrk;
      else if (HLTTYPE == HLT::JpsiTrk ) hlt_jpsitrk___ = hlt_jpsitrk;
      else throw invalid_argument("!!!! HLTTYPE not JpsiTrkTrk or JpsiTrk !!!!");

      if (!hlt_jpsimu and !hlt_jpsitrk___)
      {
        if (debug) cout << "Event does not match the triggers" << endl;
        return false;
      }

      if (debug)
      {
        cout << " +++++++++++ Passed HLT decision +++++++++++ " << endl;
      }
    }

    // SEARCH FOR CANDIDATES
    int     bestSV = -1;
    double  bestVtxProb = 0.;
    int     nBs[2] = {0, 0};

    for (int iSV = 0; iSV < nSVertices; ++iSV)
    {

      // VERTEX SEARCH
      if ((channel == CHANNEL::Bs
        or channel == CHANNEL::Bs_genBd
        or channel == CHANNEL::Bs_genLambda)
        and svtType->at(iSV) != PDEnumString::svtBsJPsiPhi) continue;

      if (channel == CHANNEL::Bd
          and svtType->at(iSV) != PDEnumString::svtBdJPsiKx) continue;

      if (debug) cout << " +++++++++++ SV found +++++++++++ " << endl;

      int iJPsi = (daugSVsFromVtx(iSV, PDEnumString::vtrCascade)).at(0);
      int iPhi  = (daugSVsFromVtx(iSV, PDEnumString::vtrCascade)).at(1); // In case of Bd this is a kstar

      // Checks
      if (svtType->at(iJPsi) != PDEnumString::svtJPsi)
      {
        cout << "(subVtxFromSV NOT A JPSI but " << svtType->at(iJPsi) << endl;
        continue;
      }
      if ((channel == CHANNEL::Bs
        or channel == CHANNEL::Bs_genBd
        or channel == CHANNEL::Bs_genLambda)
        and svtType->at(iPhi) != PDEnumString::svtPhi)
      {
        cout << "(subVtxFromSV NOT A PHI but " << svtType->at(iPhi) << endl;
        continue;
      }
      if (channel == CHANNEL::Bd
          and svtType->at(iPhi) != PDEnumString::svtKx0)
      {
        cout << "(subVtxFromSV NOT A K0* but " << svtType->at(iPhi) << endl;
        continue;
      }

      const vector<int>& mu_idx    = tracksFromSV(iJPsi);
      const vector<int>& kaon_idx  = tracksFromSV(iPhi);

      // HLT MATCH
      bool MuMatchHLT = false, KMatchHLT = false;
      int  iTkMatch = -1; // for JpsiTrk selection
      int m1 = -1, m2 = -1; //hlt indices
      int k1 = -1, k2 = -1;

      m1 = HLTMatch(mu_idx[0], true); // true = muon, false = tracks
      m2 = HLTMatch(mu_idx[1], true);
      k1 = HLTMatch(kaon_idx[0], false);
      k2 = HLTMatch(kaon_idx[1], false);

      // Muon match
      if (m1 != m2 and m1 >= 0 and m2 >= 0)  MuMatchHLT = true;

      // Traks match
      if (HLTTYPE == HLT::JpsiTrkTrk)
      {
        if (k1 != k2 and k1 >= 0 and k2 >= 0) KMatchHLT = true;
      } else if (HLTTYPE == HLT::JpsiTrk)
      {
        if (k1 >= 0)
        {
          KMatchHLT = true;
          iTkMatch = 0;
        } else if (k2 >= 0)
        {
          KMatchHLT = true;
          iTkMatch = 1;
        }
      }

      if (debug)
      {
        cout << " +++++++++++ HLT match " << MuMatchHLT << " " << KMatchHLT
             << " +++++++++++ " << endl;
      }

      // Match (common between HLT)
      if (!MuMatchHLT) continue;

      // MASS SELECTION (common between HLT)
      if (applyOfflineCuts)
      {
        if (svtMass->at(iSV) < bMassMin or svtMass->at(iSV) > bMassMax) continue;
        if (abs(svtMass->at(iJPsi) - JPSIMASS) > jpsiMassWin) continue;
        if (abs(svtMass->at(iPhi) - phiKxMass) > phiMassWin) continue;
      }

      double vtxProb     = TMath::Prob(svtChi2->at(iSV),   svtNDOF->at(iSV));
      double vtxProbJpsi = TMath::Prob(svtChi2->at(iJPsi), svtNDOF->at(iJPsi));
      double vtxProbPhi  = TMath::Prob(svtChi2->at(iPhi),  svtNDOF->at(iPhi));

      // VTX PROB SELECTION (common between HLT)
      if (applyOfflineCuts and vtxProb < vtxProbMin) continue;

      if (debug)
      {
        cout << " +++++++++++ Tentative candidate found " << iSV
             << " +++++++++++ " << endl;
      }

      // BEGINNING OF RECONSTRUCTION WITH REFITTED TRACKS
      // Index
      vector<int> mu_idx_refit, kaon_idx_refit;
      int muP_idx, muM_idx, kaonP_idx, kaonM_idx;
      int muP_idx_refit, muM_idx_refit, kaonP_idx_refit, kaonM_idx_refit;

      // Hits patterns
      int muP_hp, muM_hp, kaonP_hp, kaonM_hp;

      // 4-vectors
      PtEtaPhiMVector muP, muM, kaonP, kaonM;
      PtEtaPhiMVector muP_norefit, muM_norefit, kaonP_norefit, kaonM_norefit;
      PxPyPzEVector tBs, tJpsi, tPhi, tBsNoRefit, tJpsiNoRefit, tPhiNoRefit;

      // Refitted tracks
      for (auto it : mu_idx)   mu_idx_refit.push_back(vpIndex(it, iSV));

      for (auto it : kaon_idx) kaon_idx_refit.push_back(vpIndex(it, iSV));

      // Checks
      if (kaon_idx.size() != 2 or mu_idx.size() != 2
          or kaon_idx_refit.size() != 2 or mu_idx_refit.size() != 2 )
      {
        cout << " PROBLEMS WITH TRACKS.SIZE()" << endl;
        continue;
      }

      if ( kaon_idx_refit[0] >= int(tvpPt->size())
        or kaon_idx_refit[1] >= int(tvpPt->size())
        or mu_idx_refit[0] >= int(tvpPt->size())
        or mu_idx_refit[1] >= int(tvpPt->size()) )
      {
        cout << "PROBLEM IN THE vpIndex FUNCTION in run " << runNumber
             << " event " << eventNumber
             << " lumi " << lumiSection
             << endl;
        cout << "Size of the full track collection " << nTracks
             << " N refitted " << tvpPt->size() << endl;
        cout << " refitted indices k1 " <<  kaon_idx_refit[0]
             << " k2 " << kaon_idx_refit[1]
             << " m1 " << mu_idx_refit[0]
             << " m2 " <<  mu_idx_refit[1]
             << endl;
        continue;
      }

      // MUON ANALYSIS
      if (trkCharge->at(mu_idx[0]) > 0 and trkCharge->at(mu_idx[1]) < 0)
      {
        muP_idx = mu_idx[0];
        muM_idx = mu_idx[1];
        muP_idx_refit = mu_idx_refit[0];
        muM_idx_refit = mu_idx_refit[1];
      } else if (trkCharge->at(mu_idx[0]) < 0 and trkCharge->at(mu_idx[1]) > 0)
      {
        muP_idx = mu_idx[1];
        muM_idx = mu_idx[0];
        muP_idx_refit = mu_idx_refit[1];
        muM_idx_refit = mu_idx_refit[0];
      } else
      {
        cout << " PROBLEMS WITH MUONS CHARGES " << endl;
        continue;
      }

      muP_norefit = PtEtaPhiMVector(trkPt->at(muP_idx),
                                    trkEta->at(muP_idx),
                                    trkPhi->at(muP_idx),
                                    MUMASS);

      muM_norefit = PtEtaPhiMVector(trkPt->at(muM_idx),
                                    trkEta->at(muM_idx),
                                    trkPhi->at(muM_idx),
                                    MUMASS);

      muP = PtEtaPhiMVector(tvpPt->at(muP_idx_refit),
                            tvpEta->at(muP_idx_refit),
                            tvpPhi->at(muP_idx_refit),
                            MUMASS);

      muM = PtEtaPhiMVector(tvpPt->at(muM_idx_refit),
                            tvpEta->at(muM_idx_refit),
                            tvpPhi->at(muM_idx_refit),
                            MUMASS);

      muP_hp = trkHitPattern->at(muP_idx);
      muM_hp = trkHitPattern->at(muM_idx);

      // Muon discriminators
      bool muonSoft   = true;
      bool muonLoose  = true;
      bool muonMedium = true;

      for (auto it : mu_idx)
      {
        if (!isTrkHighPurity(it)) muonSoft = false;
        if (abs(trkDz->at(it)) > 20) muonSoft = false;
        if (abs(trkDxy->at(it)) > 0.3) muonSoft = false;
        if ((((int(trkHitPattern->at(it)) / 100) % 10000) % 100) < 5) muonSoft = false;
        if (MuonFromTrack(it) >= 0)
        {
          bool goodMuTMO = ( muoType->at( MuonFromTrack(it) ) ) & PDEnumString::tmOneStation;
          if (!goodMuTMO) muonSoft = false;
        } else
        {
          muonSoft = false;
        }
      }

      for (auto it : mu_idx)
      {
        if (MuonFromTrack(it) < 0)
        {
          muonLoose = false;
          muonMedium = false;
        }
        else
        {
          bool loose = ( muoType->at( MuonFromTrack(it) ) ) & PDEnumString::loose;
          bool med   = ( muoType->at( MuonFromTrack(it) ) ) & PDEnumString::medium;
          if (!loose) muonLoose = false;
          if (!med)   muonMedium = false;
        }
      }

      // Read Hlt Muon Pt
      double muP_hltpt = -1., muM_hltpt = -1.;

      if (MuMatchHLT)
      {
        int mp = HLTMatch(muP_idx, true);
        int mm = HLTMatch(muM_idx, true);

        muP_hltpt = hltPt->at(mp);
        muM_hltpt = hltPt->at(mm);
      }

      //TRACK ANALYSIS
      if (trkCharge->at(kaon_idx[0]) > 0 and trkCharge->at(kaon_idx[1]) < 0)
      {
        kaonP_idx = kaon_idx[0];
        kaonM_idx = kaon_idx[1];
        kaonP_idx_refit = kaon_idx_refit[0];
        kaonM_idx_refit = kaon_idx_refit[1];
      } else if (trkCharge->at(kaon_idx[0]) < 0 and trkCharge->at(kaon_idx[1]) > 0)
      {
        kaonP_idx = kaon_idx[1];
        kaonM_idx = kaon_idx[0];
        kaonP_idx_refit = kaon_idx_refit[1];
        kaonM_idx_refit = kaon_idx_refit[0];
      } else
      {
        cout << " PROBLEMS WITH TRACKS CHARGES " << endl;
        continue;
      }

      // Kstar reconstruction
      double mP = KMASS, mM = KMASS;
      int BdFlavour = 0;

      if (channel == CHANNEL::Bd)
      {

        kaonP = PtEtaPhiMVector(tvpPt->at(kaonP_idx_refit),
                                tvpEta->at(kaonP_idx_refit),
                                tvpPhi->at(kaonP_idx_refit),
                                KMASS);

        kaonM = PtEtaPhiMVector(tvpPt->at(kaonM_idx_refit),
                                tvpEta->at(kaonM_idx_refit),
                                tvpPhi->at(kaonM_idx_refit),
                                PIMASS);

        auto candKP = kaonP + kaonM;

        kaonP = PtEtaPhiMVector(tvpPt->at(kaonP_idx_refit),
                                tvpEta->at(kaonP_idx_refit),
                                tvpPhi->at(kaonP_idx_refit),
                                PIMASS);

        kaonM = PtEtaPhiMVector(tvpPt->at(kaonM_idx_refit),
                                tvpEta->at(kaonM_idx_refit),
                                tvpPhi->at(kaonM_idx_refit),
                                KMASS);

        auto candPK = kaonP + kaonM;

        //select the mass hypothesis closer to PDG value
        if (abs(candKP.M() - KXMASS) <= abs(candPK.M() - KXMASS))
        {
          mP = KMASS;
          mM = PIMASS;
          BdFlavour = 1;
        } else
        {
          mP = PIMASS;
          mM = KMASS;
          BdFlavour = -1;
        }
      }

      kaonP_norefit = PtEtaPhiMVector(trkPt->at(kaonP_idx),
                                      trkEta->at(kaonP_idx),
                                      trkPhi->at(kaonP_idx),
                                      mP);

      kaonM_norefit = PtEtaPhiMVector(trkPt->at(kaonM_idx),
                                      trkEta->at(kaonM_idx),
                                      trkPhi->at(kaonM_idx),
                                      mM);

      kaonP = PtEtaPhiMVector(tvpPt->at(kaonP_idx_refit),
                              tvpEta->at(kaonP_idx_refit),
                              tvpPhi->at(kaonP_idx_refit),
                              mP);

      kaonM = PtEtaPhiMVector(tvpPt->at(kaonM_idx_refit),
                              tvpEta->at(kaonM_idx_refit),
                              tvpPhi->at(kaonM_idx_refit),
                              mM);

      kaonP_hp = trkHitPattern->at(kaonP_idx);
      kaonM_hp = trkHitPattern->at(kaonM_idx);

      // BUILDING CANDIDATES
      if (debug)
      {
        cout << " +++++++++++ Building Candidates +++++++++++ " << endl;
        cout << "From refitted tracks: m(J/psi) " << (muP + muM).M()
             << ", m(Phi/KStar) " << (kaonP + kaonM).M() << endl;
        cout << "From native tracks: m(J/psi) "
             << (muP_norefit + muM_norefit).M()
             << ", m(Phi/Kstar) " << (kaonP_norefit + kaonM_norefit).M()
             << endl;
        cout << endl;
      }

      tJpsi         = muP + muM;
      tPhi          = kaonP + kaonM;
      tBs           = tJpsi + tPhi;
      tJpsiNoRefit  = muP_norefit + muM_norefit;
      tPhiNoRefit   = kaonP_norefit + kaonM_norefit;
      tBsNoRefit    = tJpsiNoRefit + tPhiNoRefit;

      // PRIMARY VERTEX
      int iPVpointRefit = pvRefittedForSV( iSV, PDEnumString::vtrFromAngle,
                                           PDEnumString::refittedTrkSubtract ); // this index points to the SV block
      int iPVdistRefit  = pvRefittedForSV( iSV, PDEnumString::vtrFromDist,
                                           PDEnumString::refittedTrkSubtract ); // this index points to the SV block

      int iPVpoint      = pvFromRecoForSV( iSV,
                                           PDEnumString::vtrFromAngle );   // this index points to the PV block
      int iPVdist       = pvFromRecoForSV( iSV,
                                           PDEnumString::vtrFromDist );    // this index points to the PV block

      if (iPVdist < 0)
      {
        cout << "PV not found" << endl;
        continue;
      }

      // DECAY TIME
      if (debug)
      {
        cout << "  ++++++++++++++ Decay time ++++++++++++++ " << endl;
      }

      double ctPVpointRefit    = -999.;
      double ctPVpointRefitErr = -999.;
      double ctPVdistRefit     = -999.;
      double ctPVdistRefitErr  = -999.;
      double ctPVpoint         = -999.;
      double ctPVpointErr      = -999.;

      // Ref PV
      double ctPVdist    = GetCt2D(tBs, bMass, iSV, iPVdist, false);
      double ctPVdistErr = GetCt2DErr(tBs, bMass, iSV, iPVdist, false);

      // Beamspot
      double ctBS    = GetCt2D(tBs, bMass, iSV);
      double ctBSErr = GetCt2DErr(tBs, bMass, iSV);

      if (iPVpointRefit >= 0)
      {
        ctPVpointRefit    = GetCt2D(tBs, bMass, iSV, iPVpointRefit, true);
        ctPVpointRefitErr = GetCt2DErr(tBs, bMass, iSV, iPVpointRefit, true);
      }

      if (iPVdistRefit >= 0)
      {
        ctPVdistRefit    = GetCt2D(tBs, bMass, iSV, iPVdistRefit, true);
        ctPVdistRefitErr = GetCt2DErr(tBs, bMass, iSV, iPVdistRefit, true);
      }

      if (iPVpoint >= 0)
      {
        ctPVpoint    = GetCt2D(tBs, bMass, iSV, iPVpoint, false);
        ctPVpointErr = GetCt2DErr(tBs, bMass, iSV, iPVpoint, false);
      }

      if (debug)
      {
        cout << "PV from angle refit: " << 10000. * ctPVpointRefit
             << " +/- " << 10000. * ctPVpointRefitErr << endl;
        cout << "PV from dist refit: " << 10000. * ctPVdistRefit
             << " +/- " << 10000. * ctPVdistRefitErr << endl;
        cout << "PV from angle: " << 10000. * ctPVpoint
             << " +/- " << 10000. * ctPVpointErr << endl;
        cout << "PV from dist: " << 10000. * ctPVdist
             << " +/- " << 10000. * ctPVdistErr << endl;
        cout << "BS: " << 10000. * ctBS
             << " +/- " << 10000. * ctBSErr << endl;
      }

      double ct = ctPVdist;
      double ctErr = ctPVdistErr;

      // SELECTION
      // HLT_JpsiMuon selection
      SetJpsiMuCut(applyCtCuts);
      bool passJpsiMuSel = hlt_jpsimu                                           // HLT
        and  tBs.Pt() > bsPtMin                                                 // Bs pt
        and  muP.Pt() > muPtMin_jpsimu and muM.Pt() > muPtMin_jpsimu            // Mu pt
        and  kaonP.Pt() > kaonPtMin_jpsimu and kaonM.Pt() > kaonPtMin_jpsimu    // Kaon pt
        and  ((int(kaonP_hp) / 100) % 10000) % 100 >= kaonHitsMin               // Kaon hits
        and  ((int(kaonM_hp) / 100) % 10000) % 100 >= kaonHitsMin               // Kaon hits
        and  abs(muP.Eta()) < muEtaMax and abs(muM.Eta()) < muEtaMax           // Mu eta
        and  abs(kaonP.Eta()) < kaonEtaMax and abs(kaonM.Eta()) < kaonEtaMax    // Kaon eta
        and  ct > ctMin                                                         // ct
        and  MuMatchHLT == 1                                                    // HLT match
        and  muonLoose == true;                                                 // Muon ID

      bool passJpsiMuSel_HLT = hlt_jpsimu
                               and muP.Pt() > muPt_HLTmu
                               and muM.Pt() > muPt_HLTmu;

      // HLT_JpsiTrkTrk selection
      SetJpsiTrkTrkCut(applyCtCuts);

      bool passJpsiTrkTrkSel = hlt_jpsitrk___                                   // HLT
        and tBs.Pt() > bsPtMin                                                  // Bs pt
        and muP.Pt() > muPtMin_jpsitrktrk and muM.Pt() > muPtMin_jpsitrktrk     // Mu pt
        and tJpsi.Pt() > jpsiPt_HLTtk                                           // HLT jpsi selection
        and kaonP.Pt() > kaonPtMin_jpsitrktrk                                   // KaonP pt
        and kaonM.Pt() > kaonPtMin_jpsitrktrk                                   // KaonM pt
        and ((int(kaonP_hp) / 100) % 10000) % 100 >= kaonHitsMin                // Kaon hits
        and ((int(kaonM_hp) / 100) % 10000) % 100 >= kaonHitsMin                // Kaon hits
        and abs(muP.Eta()) < muEtaMax and abs(muM.Eta()) < muEtaMax             // Mu eta
        and abs(kaonP.Eta()) < kaonEtaMax and abs(kaonM.Eta()) < kaonEtaMax     // Kaon eta
        and ct > ctMin                                                          // ct
        and ct / ctErr > ctSigmaMin                                             // HLT displacement
        and MuMatchHLT == 1 and KMatchHLT == 1                                  // HLT match
        and muonLoose == true;                                                  // Muon ID

      if (HLTTYPE == HLT::JpsiTrk and iTkMatch != -1)
      {
        passJpsiTrkTrkSel = passJpsiTrkTrkSel
                            and tvpPt->at(kaon_idx_refit[iTkMatch]) > 1.2;      // Reproduce HLT cut
      }

      bool passJpsiTrkTrkSel_HLT = hlt_jpsitrk___;

      if (HLTTYPE == HLT::JpsiTrkTrk)
      {
        passJpsiTrkTrkSel_HLT = passJpsiTrkTrkSel_HLT
                                and muP.Pt() > muPt_HLTtk
                                and muM.Pt() > muPt_HLTtk
                                and tJpsi.Pt() > jpsiPt_HLTtk
                                and kaonP.Pt() > kaonPt_HLTtk
                                and kaonM.Pt() > kaonPt_HLTtk
                                and ct / ctErr > ctSigmaMin_HLTtk;
      }

      if (HLTTYPE == HLT::JpsiTrk and iTkMatch != -1)
      {
        passJpsiTrkTrkSel_HLT = passJpsiTrkTrkSel_HLT
                                and tvpPt->at(kaon_idx_refit[iTkMatch]) > 1.2;
      }

      if (debug)
      {
        cout << "selections: " << endl;
        cout << "hlt_jpsimu: " << hlt_jpsimu
             << " => " << boolalpha << (hlt_jpsimu == 1) << endl;               // HLT
        cout << "hlt_jpsitrk___: " << hlt_jpsitrk___
             << " => " << boolalpha << (hlt_jpsitrk___ == 1) << endl;           // HLT
        cout << "tBs.Pt(): " << tBs.Pt()
             << " => " << boolalpha << (tBs.Pt() > bsPtMin) << endl;            // Bs pt
        cout << "muP.Pt(): " << muP.Pt()
             << " => " << boolalpha << (muP.Pt() > muPtMin) << endl;            // Mu pt
        cout << "muM.Pt(): " << muM.Pt()
             << " => " << boolalpha << (muM.Pt() > muPtMin) << endl;            // Mu pt
        cout << "tJpsi.Pt(): " << tJpsi.Pt()
             << " => " << boolalpha << (tJpsi.Pt() > jpsiPt_HLTtk) << endl;     // Jpsi hlt selection
        cout << "kaonP.Pt(): " << kaonP.Pt()
             << " => " << boolalpha << (kaonP.Pt() > kaonPtMin) << endl;        // Kaon pt
        cout << "kaonM.Pt(): " << kaonM.Pt()
             << " => " << boolalpha << (kaonM.Pt() > kaonPtMin) << endl;        // Kaon pt
        cout << "kaonP_hp: " << kaonP_hp
             << " => " << ((int(kaonP_hp) / 100) % 10000) % 100
             << " => " << boolalpha << (((int(kaonP_hp) / 100) % 10000) % 100 >= kaonHitsMin) << endl; // Kaon hits
        cout << "kaonM_hp: " << kaonM_hp
             << " => " << ((int(kaonP_hp) / 100) % 10000) % 100
             << " => " << boolalpha << (((int(kaonM_hp) / 100) % 10000) % 100 >= kaonHitsMin) << endl; // Kaon hits
        cout << "abs(muP.Eta()): " << abs(muP.Eta())
             << " => " << boolalpha << (abs(muP.Eta()) < muEtaMax) << endl;     // Mu eta
        cout << "abs(muM.Eta()): " << abs(muM.Eta())
             << " => " << boolalpha << (abs(muM.Eta()) < muEtaMax) << endl;     // Mu eta
        cout << "abs(kaonP.Eta()): " << abs(kaonP.Eta())
             << " => " << boolalpha << (abs(kaonP.Eta()) < kaonEtaMax) << endl; // Kaon eta
        cout << "abs(kaonM.Eta()): " << abs(kaonM.Eta())
             << " => " << boolalpha << (abs(kaonM.Eta()) < kaonEtaMax) << endl; // Kaon eta
        cout << "ct/ctErr: " << ct << "/" << ctErr << " = " << ct / ctErr
             << " => " << boolalpha << (ct / ctErr > ctSigmaMin) << endl;       // sigma ct
        cout << "ct: " << ct
             << " => " << boolalpha << (ct > ctMin) << endl;                    // ct
        cout << "MuMatchHLT: " << MuMatchHLT
             << " => " << boolalpha << (MuMatchHLT == 1) << endl;               // HLT match
        cout << "KMatchHLT: " << KMatchHLT
             << " => " << boolalpha << (KMatchHLT == 1) << endl;                // HLT match
        cout << "muonLoose: " << muonLoose
             << " => " << boolalpha << (muonLoose == true) << endl;             // muon loose
      }

      if (applyOfflineCuts and !passJpsiMuSel and !passJpsiTrkTrkSel) continue;

      if (!applyOfflineCuts
          and (channel == CHANNEL::Bs
               or channel == CHANNEL::Bs_genBd
               or channel == CHANNEL::Bs_genLambda)
          and !passJpsiMuSel_HLT
          and !passJpsiTrkTrkSel_HLT) continue;

      if (debug)
      {
        cout << "++++++++++++++ Candidate found " << iSV
             << " ++++++++++++++ " << endl;
      }

      if (passJpsiMuSel) nBs[0]++;
      if (passJpsiTrkTrkSel and !passJpsiMuSel) nBs[1]++;

      if (vtxProb < bestVtxProb) continue;
      bestVtxProb = vtxProb;
      bestSV = iSV;

      // TAGGING
      // OsMuon
      setVtxOsMuonTag(iSV, iPVdist);
      int osMuonTagDecision = makeOsMuonTagging(true); //(useDnnTag)

      (rWriter->tag_Mu)        = osMuonTagDecision;
      (rWriter->mistag_Mu1)    = getOsMuonTagMistagProb1();
      (rWriter->mistag_Mu2)    = getOsMuonTagMistagProb2();
      (rWriter->mistagCal_Mu1) = getOsMuonTagMistagProbCal1();
      (rWriter->mistagCal_Mu2) = getOsMuonTagMistagProbCal2();

      // OsEle
      setVtxOsEleTag(iSV, iPVdist);
      int osEleTagDecision     = makeOsEleTagging(true, false); //(useDnnTag, vetoMu)
      (rWriter->tag_Ele)       = osEleTagDecision;
      (rWriter->mistag_Ele)    = getOsEleTagMistagProb();
      (rWriter->mistagCal_Ele) = getOsEleTagMistagProbCal();

      // OsJet
      setVtxOSJetTag(iSV, iPVdist);
      int osJetTagDecision     = makeOSJetTagging(false, false); //(vetoMu, vetoEle)
      (rWriter->tag_Jet)       = osJetTagDecision;
      (rWriter->mistag_Jet)    = getOSJetTagMistagProb();
      (rWriter->mistagCal_Jet) = getOSJetTagMistagProbCal();

      // SameSide
      setVtxSSTag(iSV, iPVdist);
      int ssTagDecision       = makeSSTagging(SSVetoMode::NoVeto,
                                              SSVetoMode::NoVeto,
                                              SSVetoMode::NoVeto); //(vetoMu, vetoEle, vetoJet)
      (rWriter->tag_SS)       = ssTagDecision;
      (rWriter->mistag_SS)    = getSSTagMistagProb();
      (rWriter->mistagCal_SS) = getSSTagMistagProbCal();
      (rWriter->tag_SS_commonTrk) = static_cast<int>(getSSTagCommonVeto());

      if (channel == CHANNEL::Bd)
      {
        (rWriter->tag_BdMass) = BdFlavour;
      }

      // ANGULAR VARIABLES
      if (debug)
      {
        cout << "  ++++++++++++++ Computing Angles ++++++++++++++ " << endl;
      }

      // Angular variables
      double angle_costheta, angle_phi, angle_cospsi;
      // the betas for the boost
      auto vJpsi = tJpsi.BoostToCM();

      // the boost matrix
      Boost boost_Jpsi(vJpsi);
      //auto tJpsi_Jpsi = boost_Jpsi(tJpsi); // FIXME unused?

      // the different momenta in the new frame
      auto tJpsi_MuP    = boost_Jpsi(muP);
      auto tJpsi_KaonP  = boost_Jpsi(kaonP);
      auto tJpsi_Phi    = boost_Jpsi(tPhi);

      // the 3-momenta
      XYZVector vJpsi_MuP    = tJpsi_MuP.Vect();
      XYZVector vJpsi_KaonP  = tJpsi_KaonP.Vect();
      XYZVector vJpsi_Phi    = tJpsi_Phi.Vect();

      // coordinate system
      XYZVector x, y, z;
      x = vJpsi_Phi.Unit();
      y = vJpsi_KaonP.Unit() - (x * (x.Dot(vJpsi_KaonP.Unit())));
      y = y.Unit();
      z = x.Cross(y);

      // Transversity Basis
      angle_costheta = vJpsi_MuP.Unit().Dot(z);

      double cos_phi = vJpsi_MuP.Unit().Dot(x)
                       / TMath::Sqrt(1 - angle_costheta * angle_costheta);

      double sin_phi = vJpsi_MuP.Unit().Dot(y)
                       / TMath::Sqrt(1 - angle_costheta * angle_costheta);

      angle_phi = TMath::ACos(cos_phi);
      if (sin_phi < 0)
      {
        angle_phi =  -angle_phi;
      }

      // boosting in phi rest frame
      // the betas for the boost
      auto vPhi = tPhi.BoostToCM();

      // the boost matrix
      Boost boost_Phi(vPhi);
      //auto tPhi_Phi = boost_Phi(tPhi); // FIXME unused?

      // the different momenta in the new frame
      auto tPhi_KaonP  = boost_Phi(kaonP);
      auto tPhi_Jpsi   = boost_Phi(tJpsi);
      //auto tPhi_Bs     = boost_Phi(tBs); // FIXME unused?

      // the 3-momenta
      XYZVector vPhi_KaonP = tPhi_KaonP.Vect();
      XYZVector vPhi_Jpsi  = tPhi_Jpsi.Vect();
      //XYZVector vPhi_Bs    = tPhi_Bs.Vect(); // FIXME unused?

      angle_cospsi = -1 * vPhi_KaonP.Unit().Dot(vPhi_Jpsi.Unit());

      // for Bd signs are different and depend on the flavour
      // https://journals.aps.org/prd/pdf/10.1103/PhysRevD.88.052002
      if (channel == CHANNEL::Bd)
      {
        angle_cospsi = BdFlavour * angle_cospsi;
        angle_costheta = BdFlavour * angle_costheta;
      }

      if (debug)
      {
        cout << "CosPsi " << angle_cospsi
             << " CosTheta " << angle_costheta
             << " Angle Phi " << angle_phi
             << endl;
      }

      // FILLING OUTPUT
      if (debug)
      {
        cout << "  +++++++++++ Filling tree +++++++++++ " << endl;
      }

      bool isbs = ((channel == CHANNEL::Bs
                    or channel == CHANNEL::Bs_genBd
                    or channel == CHANNEL::Bs_genLambda))
                  ? true : false;

      (rWriter->hlt_JpsiMu)     = hlt_jpsimu;
      (rWriter->hlt_JpsiTrkTrk) = hlt_jpsitrktrk;
      (rWriter->hlt_JpsiTrk)    = hlt_jpsitrk;

      (rWriter->passJpsiMuSel)     = passJpsiMuSel;
      (rWriter->passJpsiTrkTrkSel) = passJpsiTrkTrkSel;

      (rWriter->run)  = runNumber;
      (rWriter->evt)  = ULong64_t(eventNumber);
      (rWriter->lumi) = lumiSection;
      (rWriter->nPV)  = nPVertices;

      (rWriter->isBs)             = isbs;
      (rWriter->hltMatch_Mu)      = MuMatchHLT;
      (rWriter->hltMatch_Tracks)  = KMatchHLT;

      // (rWriter->pv_Pointing)      = pointingAngle;

      (rWriter->bs_MassFromSV)    = svtMass->at(iSV);
      (rWriter->jpsi_MassFromSV)  = svtMass->at(iJPsi);
      (rWriter->phi_MassFromSV)   = svtMass->at(iPhi);

      (rWriter->pv_VtxProb)   = vtxProb;
      (rWriter->jpsi_VtxProb) = vtxProbJpsi;
      (rWriter->phi_VtxProb)  = vtxProbPhi;

      (rWriter->muonSoftID)     = muonSoft;
      (rWriter->muonLooseID)    = muonLoose;
      (rWriter->muonMediumID)   = muonMedium;

      (rWriter->angle_CosPsi)   = angle_cospsi;
      (rWriter->angle_CosTheta) = angle_costheta;
      (rWriter->angle_Phi)      = angle_phi;

      (rWriter->ctBS)               = ctBS;
      (rWriter->ctBS_Err)           = ctBSErr;
      (rWriter->ctPVpointRefit)     = ctPVpointRefit;
      (rWriter->ctPVpointRefit_Err) = ctPVpointRefitErr;
      (rWriter->ctPVdistRefit)      = ctPVdistRefit;
      (rWriter->ctPVdistRefit_Err)  = ctPVdistRefitErr;
      (rWriter->ctPVpoint)          = ctPVpoint;
      (rWriter->ctPVpoint_Err)      = ctPVpointErr;
      (rWriter->ctPVdist)           = ctPVdist;
      (rWriter->ctPVdist_Err)       = ctPVdistErr;

      // (rWriter->bs_Mass)          = tBs.M();
      (rWriter->bs_Pt)  = tBs.Pt();
      (rWriter->bs_Eta) = tBs.Eta();
      (rWriter->bs_Phi) = tBs.Phi();

      // (rWriter->bs_MassNoRefit)   = tBsNoRefit.M();
      // (rWriter->jpsi_MassNoRefit) = tJpsiNoRefit.M();
      // (rWriter->phi_MassNoRefit)  = tPhiNoRefit.M();

      // (rWriter->jpsi_Mass)        = tJpsi.M();
      (rWriter->jpsi_Pt)  = tJpsi.Pt();
      (rWriter->jpsi_Eta) = tJpsi.Eta();
      (rWriter->jpsi_Phi) = tJpsi.Phi();

      // (rWriter->phi_Mass)         = tPhi.M();
      (rWriter->phi_Pt)   = tPhi.Pt();
      (rWriter->phi_Eta)  = tPhi.Eta();
      (rWriter->phi_Phi)  = tPhi.Phi();

      (rWriter->muP_Pt)     = muP.Pt();
      (rWriter->muP_Eta)    = muP.Eta();
      (rWriter->muP_Phi)    = muP.Phi();
      (rWriter->muP_Hits)   = muP_hp;
      (rWriter->muP_HltPt)  = muP_hltpt;

      (rWriter->muM_Pt)     = muM.Pt();
      (rWriter->muM_Eta)    = muM.Eta();
      (rWriter->muM_Phi)    = muM.Phi();
      (rWriter->muM_Hits)   = muM_hp;
      (rWriter->muM_HltPt)  = muM_hltpt;

      (rWriter->kaonP_Pt)   = kaonP.Pt();
      (rWriter->kaonP_Eta)  = kaonP.Eta();
      (rWriter->kaonP_Phi)  = kaonP.Phi();
      (rWriter->kaonP_Hits) = kaonP_hp;

      (rWriter->kaonM_Pt)   = kaonM.Pt();
      (rWriter->kaonM_Eta)  = kaonM.Eta();
      (rWriter->kaonM_Phi)  = kaonM.Phi();
      (rWriter->kaonM_Hits) = kaonM_hp;

      // Needed for gen block
      tMuP_reco = muP;
      tMuM_reco = muM;
      tKaonP_reco = kaonP;
      tKaonM_reco = kaonM;

    }

    if (bestSV == -1)
    {
      if (debug)
      {
        cout << "No candidate found" << endl;
      }
      return false; // skip events if no candidate was found
    }

    (rWriter->nBs_JpsiMu) = nBs[0];
    (rWriter->nBs_JpsiTrkTrk) = nBs[1];

  }

  if (use_gen or genOnly)
  {
    if (debugGEN)
    {
      cout << "  ++++++++++++++ Begin GEN INFO " << nGenP
           << " ++++++++++++++ " << endl;
    }

    // Lund codes here:
    // http://research.npl.illinois.edu/exp/eic/bootcamp/gmc1/lundcodes.html
    int processLund[4]; // B lund, jpsi lund, phi/kstar lund codes

    if (channel == CHANNEL::Bs)
    {
      processLund[0] = 531;
      processLund[1] = 443;
      processLund[2] = 333;
      processLund[3] = 0; // Dummy
    }

    if (channel == CHANNEL::Bd or channel == CHANNEL::Bs_genBd)
    {
      processLund[0] = 511;
      processLund[1] = 443;
      processLund[2] = 313;
      processLund[3] = 0; // Dummy
    }

    if (channel == CHANNEL::Bs_genLambda)
    {
      processLund[0] = 5122;
      processLund[1] = 443;
      processLund[2] = 2212;
      processLund[3] = 321;
    }

    int bsIndex = -1, jpsiIndex = -1, phiIndex = -1, thirdIndex = -1;
    int kaonPIndex = -1, kaonMIndex = -1, muPIndex = -1, muMIndex = -1;
    int bsMix = -1;
    int genFlavour = 0;
    int genFlavourDecay = 0; // B flavour at decay time. Affected by mixing

    // SEARCH GEN
    for (int p = 0; p < nGenP; ++p)
    {
      bsIndex     = -1;
      jpsiIndex   = -1;
      phiIndex    = -1;
      kaonPIndex  = -1;
      kaonMIndex  = -1;
      muPIndex    = -1;
      muMIndex    = -1;
      bsMix       = -1;

      if (abs(genId->at(p)) != processLund[0]) continue; // skip if not B of choice

      bsMix = GetMixStatus(p);
      if (bsMix == 2) continue;
      // 2 -> B going to change flavor (removed to avoid double counting)
      // 1 -> B has changed flavor
      // 0 -> no flavor changed

      if (debugGEN)
      {
        cout << " Found B candidate " << genId->at(p) << endl;
      }

      const vector<int>& aD = allDaughters(p);

      for (int it : aD)
      {
        if (abs(genId->at(it)) == processLund[1]) jpsiIndex  = it;
        if (abs(genId->at(it)) == processLund[2]) phiIndex   = it;
        if (abs(genId->at(it)) == processLund[3]) thirdIndex = it;
      }

      // phiIndex = phi(1020) for Bs, Kstar for Bd and p (dummy) for lambdab0
      // thirdIndex = kaon for lambdab0 and dummy for the rest
      if (jpsiIndex == -1 or phiIndex == -1) continue;
      if (channel == CHANNEL::Bs_genLambda and thirdIndex == -1) continue;

      const vector<int>& aDjpsi = allDaughters(jpsiIndex);
      const vector<int>& aDphi  = allDaughters(phiIndex);

      // aDphi is empty for lambdab0

      if (debugGEN)
      {
        cout << " Primary daughters OK "
             << genId->at(jpsiIndex) << " " << genId->at(phiIndex) << endl;
      }

      //J/Psi muons
      for (int it : aDjpsi)
      {
        if (genId->at(it) ==  13) muMIndex = it;
        if (genId->at(it) == -13) muPIndex = it;
      }

      if ( muMIndex == -1 or muPIndex == -1 ) continue; // skip if not Jpsi->mumu

      //Phi Kaons
      if (channel == CHANNEL::Bs)
      {
        for (int it : aDphi)
        {
          if (genId->at(it) ==  321) kaonPIndex = it;
          if (genId->at(it) == -321) kaonMIndex = it;
        }
        if ( kaonPIndex == -1 or kaonMIndex == -1 ) continue; // skip if not Phi->KK
      }

      // Kx0 K/Pi
      if (channel == CHANNEL::Bd or channel == CHANNEL::Bs_genBd)
      {
        if (genId->at(phiIndex) == 313)
        {
          for (int it : aDphi)
          {
            if (genId->at(it) ==  321) kaonPIndex = it;
            if (genId->at(it) == -211) kaonMIndex = it;
          }
        } else if (genId->at(phiIndex) == -313)
        {
          for (int it : aDphi)
          {
            if (genId->at(it) ==  211) kaonPIndex = it;
            if (genId->at(it) == -321) kaonMIndex = it;
          }
        }
        if ( kaonPIndex == -1 or kaonMIndex == -1 ) continue; // skip if not Kx0->KPi
      }

      if (channel == CHANNEL::Bs_genLambda)
      {
        if (genId->at(phiIndex) > 0)
        {
          kaonPIndex = phiIndex;
          kaonMIndex = thirdIndex;
        } else
        {
          kaonPIndex = thirdIndex;
          kaonMIndex = phiIndex;
        }
      }

      if (debugGEN)
      {
        cout << "Final state OK "
             << genId->at(kaonPIndex) << " " << genId->at(kaonMIndex) << endl;
      }

      bsIndex = p;
      if (genId->at(p) > 0) genFlavour =  1;
      else                  genFlavour = -1;

      genFlavourDecay = genFlavour;
      // B0 angles depend on decay flavour.
      // After the next line genFlavour will
      // contain the production flavour instead

      if (bsMix == 1) genFlavour *= -1;

      break; // Skip as soon as one candidate has been found
    }

    (rWriter->gen_LundCode) = genFlavour;
    (rWriter->gen_IsBMixed) = bsMix;

    if (bsIndex == -1)
    {
      (rWriter->gen_GenFound) = 0;
      if (!genOnly) rWriter->fill();
      return false;
    }
    else
    {
      (rWriter->gen_GenFound) = 1;
    }

    if (debug) cout << "Found MC event, tot particles " << nGenP << " indexes: "
                      << bsIndex << " " <<  jpsiIndex << " " <<  phiIndex << " "
                      << muPIndex  << " " << muMIndex << " "
                      << kaonPIndex << " " << kaonMIndex << endl;

    // // SET GEN VERTEX POSITION
    // XYZPoint GenSV(genVx->at(jpsiIndex),genVy->at(jpsiIndex),genVz->at(jpsiIndex));
    // XYZPoint GenPV;

    // if (process=="Bs"
    //   and (genVx->at(jpsiIndex)!=genVx->at(phiIndex)
    //   or genVy->at(jpsiIndex)!=genVy->at(phiIndex)
    //   or genVz->at(jpsiIndex)!=genVz->at(phiIndex)))
    //   cout<<endl<<"!!! Jpsi GEN PV != Phi GEN PV !!!"<<endl;

    // GenSV.SetXYZ(genVx->at(jpsiIndex),genVy->at(jpsiIndex),genVz->at(jpsiIndex));
    // if (bsMix==0) {
    //   GenPV = XYZPoint(genVx->at(bsIndex),genVy->at(bsIndex),genVz->at(bsIndex));
    // } else if (bsMix==1) {
    //   const vector <int>& aM = allMothers(bsIndex);
    //   if (aM.size()>1)  cout<<"Bs with more than one mother"<<endl;
    //   GenPV = XYZPoint(genVx->at(aM[0]),genVy->at(aM[0]),genVz->at(aM[0]));
    // } else { cout<<endl<<"!!! Selected gen candidate with mix status !!!"<<bsMix<<endl;}

    PtEtaPhiMVector tBsGen(genPt->at(bsIndex),
                           genEta->at(bsIndex),
                           genPhi->at(bsIndex),
                           genMass->at(bsIndex));

    PtEtaPhiMVector tJpsiGen(genPt->at(jpsiIndex),
                             genEta->at(jpsiIndex),
                             genPhi->at(jpsiIndex),
                             genMass->at(jpsiIndex));

    PtEtaPhiMVector tPhiGen(genPt->at(phiIndex),
                            genEta->at(phiIndex),
                            genPhi->at(phiIndex),
                            genMass->at(phiIndex));

    PtEtaPhiMVector tMuPGen(genPt->at(muPIndex),
                            genEta->at(muPIndex),
                            genPhi->at(muPIndex),
                            genMass->at(muPIndex));

    PtEtaPhiMVector tMuMGen(genPt->at(muMIndex),
                            genEta->at(muMIndex),
                            genPhi->at(muMIndex),
                            genMass->at(muMIndex));

    PtEtaPhiMVector tKaonPGen(genPt->at(kaonPIndex),
                              genEta->at(kaonPIndex),
                              genPhi->at(kaonPIndex),
                              genMass->at(kaonPIndex));

    PtEtaPhiMVector tKaonMGen(genPt->at(kaonMIndex),
                              genEta->at(kaonMIndex),
                              genPhi->at(kaonMIndex),
                              genMass->at(kaonMIndex));

    if (debugGEN) cout << "Gen Masses " << tBsGen.M() << " "
                         << tJpsiGen.M() << " " << tPhiGen.M() << " "
                         << tMuPGen.M() << " " << tMuMGen.M() << " "
                         << tKaonPGen.M() << " " << tKaonMGen.M() << endl;

    // MC MATCHING
    int isMCMatched = 0;

    if (!genOnly)
    {
      if (channel == CHANNEL::Bs
          and DeltaR(tKaonPGen, tKaonP_reco) < 0.005
          and DeltaR(tKaonMGen, tKaonM_reco) < 0.005
          and DeltaR(tMuMGen, tMuM_reco) < 0.005
          and DeltaR(tMuPGen, tMuP_reco) < 0.005 )
      {
        isMCMatched =  1;
      }

      if ((channel == CHANNEL::Bd or channel == CHANNEL::Bs_genBd)
          and DeltaR(tPhiGen, tKaonP_reco + tKaonM_reco) < 0.005
          and DeltaR(tMuMGen, tMuM_reco) < 0.005
          and DeltaR(tMuPGen, tMuP_reco) < 0.005)
      {
        isMCMatched =  1;
      }

      if (channel == CHANNEL::Bs_genLambda
          and DeltaR(tBsGen, tMuM_reco + tMuP_reco + tKaonP_reco + tKaonM_reco) < 0.005
          and DeltaR(tMuMGen, tMuM_reco) < 0.005
          and DeltaR(tMuPGen, tMuP_reco) < 0.005)
      {
        isMCMatched =  1;
      }

      (rWriter->gen_Matched) = isMCMatched;

      if (debug)
      {
        cout << "Distance GEN reco: Kp " << DeltaR(tKaonPGen, tKaonP_reco)
             << " Km " <<  DeltaR(tKaonMGen, tKaonM_reco)
             << " Mum " << DeltaR(tMuMGen, tMuM_reco)
             << " Mup " << DeltaR(tMuPGen, tMuP_reco)
             << endl;

        cout << "Distance GEN reco: Kstar "
             << DeltaR(tPhiGen, tKaonP_reco + tKaonM_reco)
             << " Mum " << DeltaR(tMuMGen, tMuM_reco)
             << " Mup " << DeltaR(tMuPGen, tMuP_reco) << endl;
      }
    }

    if (debugGEN)
    {
      cout << "Match: " << isMCMatched << endl;
    }

    if (isMCMatched)  h1->Fill(tPhiGen.M());
    if (!isMCMatched) h2->Fill(tPhiGen.M());

    // GEN LIFETIME

    double GenCt3D = GetGenCT3D(bsIndex);
    double GenCt   = GetGenCT2D(bsIndex);

    if (debug)
    {
      cout << "Gen Ct3D " << GenCt3D << " Gen Ct " << GenCt
           << " bsIndex " << bsIndex << " jpsiIndex " << jpsiIndex << endl;
    }

    // COMPUTING GEN ANGLES
    double angle_costhetaGen;
    double angle_phiGen;
    double angle_cospsiGen;

    auto vJpsiGen = tJpsiGen.BoostToCM();

    // the boost matrix
    Boost boost_JpsiGen(vJpsiGen);
    //auto tJpsi_JpsiGen = boost_JpsiGen(tJpsiGen); // FIXME unused?

    // the different momenta in the new frame
    auto tJpsi_muPGen   = boost_JpsiGen(tMuPGen);
    auto tJpsi_KaonPGen = boost_JpsiGen(tKaonPGen);
    auto tJpsi_PhiGen   = boost_JpsiGen(tPhiGen);

    // the 3-momenta
    XYZVector vJpsi_muPGen   = tJpsi_muPGen.Vect();
    XYZVector vJpsi_KaonPGen = tJpsi_KaonPGen.Vect();
    XYZVector vJpsi_PhiGen   = tJpsi_PhiGen.Vect();

    // coordinate system
    XYZVector xg, yg, zg;
    xg = vJpsi_PhiGen.Unit();
    yg = vJpsi_KaonPGen.Unit() - (xg * (xg.Dot(vJpsi_KaonPGen.Unit())));
    yg = yg.Unit();
    zg = xg.Cross(yg);

    // Transversity Basis
    angle_costhetaGen = vJpsi_muPGen.Unit().Dot(zg);

    double cos_phiGen = vJpsi_muPGen.Unit().Dot(xg)
                        / TMath::Sqrt(1 - angle_costhetaGen * angle_costhetaGen);

    double sin_phiGen = vJpsi_muPGen.Unit().Dot(yg)
                        / TMath::Sqrt(1 - angle_costhetaGen * angle_costhetaGen);

    angle_phiGen = TMath::ACos(cos_phiGen);

    if (sin_phiGen < 0) angle_phiGen = - angle_phiGen;

    // boosting in phi rest frame
    // the betas for the boost
    auto vPhiGen = tPhiGen.BoostToCM();

    // the boost matrix
    Boost boost_PhiGen(vPhiGen);
    //auto tPhi_PhiGen = boost_PhiGen(tPhiGen); // FIXME unused?

    // the different momenta in the new frame
    auto tPhi_KaonPGen = boost_PhiGen(tKaonPGen);
    auto tPhi_JpsiGen  = boost_PhiGen(tJpsiGen);
    //auto tPhi_BsGen    = boost_PhiGen(tBsGen); // FIXME unused?

    // the 3-momenta
    XYZVector vPhi_KaonPGen   = tPhi_KaonPGen.Vect();
    XYZVector vPhi_tJPsiGen   = tPhi_JpsiGen.Vect();
    //XYZVector vPhi_tBsGen     = tPhi_BsGen.Vect(); // FIXME unused?

    angle_cospsiGen = -1 * vPhi_KaonPGen.Unit().Dot(vPhi_tJPsiGen.Unit());

    // for Bd signs are different and depend on the flavour
    // https://journals.aps.org/prd/pdf/10.1103/PhysRevD.88.052002
    if (channel == CHANNEL::Bd)
    {
      angle_cospsiGen = genFlavourDecay * angle_cospsiGen;
      angle_costhetaGen = genFlavourDecay * angle_costhetaGen;
    }

    if (debug)
    {
      cout << "CosPsi GEN " << angle_cospsiGen
           << " CosTheta GEN " << angle_costhetaGen
           << " Angle Phi GEN " << angle_phiGen << endl;
    }

    // GEN FILL
    (rWriter->angle_CosPsi_GEN)   = angle_cospsiGen;
    (rWriter->angle_CosTheta_GEN) = angle_costhetaGen;
    (rWriter->angle_Phi_GEN)      = angle_phiGen;

    (rWriter->ct_GEN)         = GenCt;
    (rWriter->ct3D_GEN)       = GenCt3D;

    (rWriter->bs_Mass_GEN)    = tBsGen.M();
    (rWriter->bs_Pt_GEN)      = tBsGen.Pt();
    (rWriter->bs_Eta_GEN)     = tBsGen.Eta();
    (rWriter->bs_Phi_GEN)     = tBsGen.Phi();

    (rWriter->jpsi_Mass_GEN)  = tJpsiGen.M();
    (rWriter->jpsi_Pt_GEN)    = tJpsiGen.Pt();
    (rWriter->jpsi_Eta_GEN)   = tJpsiGen.Eta();
    (rWriter->jpsi_Phi_GEN)   = tJpsiGen.Phi();

    (rWriter->phi_Mass_GEN)   = tPhiGen.M();
    (rWriter->phi_Pt_GEN)     = tPhiGen.Pt();
    (rWriter->phi_Eta_GEN)    = tPhiGen.Eta();
    (rWriter->phi_Phi_GEN)    = tPhiGen.Phi();

    (rWriter->muP_Pt_GEN)     = tMuPGen.Pt();
    (rWriter->muP_Eta_GEN)    = tMuPGen.Eta();
    (rWriter->muP_Phi_GEN)    = tMuPGen.Phi();

    (rWriter->muM_Pt_GEN)     = tMuMGen.Pt();
    (rWriter->muM_Eta_GEN)    = tMuMGen.Eta();
    (rWriter->muM_Phi_GEN)    = tMuMGen.Phi();

    (rWriter->kaonP_Pt_GEN)   = tKaonPGen.Pt();
    (rWriter->kaonP_Eta_GEN)  = tKaonPGen.Eta();
    (rWriter->kaonP_Phi_GEN)  = tKaonPGen.Phi();

    (rWriter->kaonM_Pt_GEN)   = tKaonMGen.Pt();
    (rWriter->kaonM_Eta_GEN)  = tKaonMGen.Eta();
    (rWriter->kaonM_Phi_GEN)  = tKaonMGen.Phi();
  }

  rWriter->fill();

  if (debug)
  {
    cout << "  ++++++++++++++ EVENT END ++++++++++++++ " << endl;
  }

  return true;

}


void PDAnalyzer::endJob()
{

  rWriter->close(); // close file for reduced ntuple

  cout << endl;

  for (int i = 0; i < 20; ++i) cout << counter[i] << endl;

  return;

}

void PDAnalyzer::save()
{

#  if UTIL_USE == FULL

  autoSave();
#elif UTIL_USE == BARE

#endif

  return;
}
