#!/bin/bash
i=0;
max=239;
echo $'#!/bin/sh' > park.sh
echo -n "hadd ntuBsData2017_noCut.root" >> park.sh
while [ "$i" -le "$max" ]; do
  echo -n " ./ntu/ntu$i.root" >> park.sh
  i=`expr "$i" + 1`;
done
echo " " >> park.sh
bash park.sh;
rm park.sh;
mv ntuBsData2017_noCut.root ../
