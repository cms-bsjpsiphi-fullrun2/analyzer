#!/bin/bash
i=0;
max=259;
echo $'#!/bin/sh' > park.sh
echo -n "hadd ntuBsData2017.root" >> park.sh
while [ "$i" -le "$max" ]; do
  echo -n " ./ntu/ntu$i.root" >> park.sh
  i=`expr "$i" + 1`;
done
echo " " >> park.sh
bash park.sh;
rm park.sh;
mv ntuBsData2017.root ../../
