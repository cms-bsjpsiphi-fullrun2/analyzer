#!/bin/bash
i=0;
max=9;
echo $'#!/bin/sh' > park.sh
echo -n "hadd ntuBsDG0noFilter.root" >> park.sh
while [ "$i" -le "$max" ]; do
  echo -n " ntu/ntu$i.root" >> park.sh
  i=`expr "$i" + 1`;
done
echo " " >> park.sh
bash park.sh;
rm park.sh;
mv ntuBsDG0noFilter.root ../